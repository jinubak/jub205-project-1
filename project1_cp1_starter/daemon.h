#ifndef DAEMON_H
#define DAEMON_H

#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "lisod_helper.h"

void signal_handler(int);

int daemonize(char *);

#endif
