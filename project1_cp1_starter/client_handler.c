#include <stdio.h>
#include <stdlib.h>

#include "client_handler.h"
#include "logger.h"

client_buf *init_buf()
{
        client_buf *buf = calloc(1, sizeof(client_buf));
        if (buf == NULL) {
                log_write("Out of memory\n");
                return NULL;
        }

        buf->buf = calloc(BUF_SIZE, sizeof(char));
        if (buf->buf == NULL) {
                free(buf);
                log_write("Out of memory\n");
                return NULL;
        }       
        //buffer info initialization
        buf->buf_length = 0;
        buf->buf_size = BUF_SIZE;
        buf->buf_p = buf->buf;
        return buf;
}

int resize_buf(client_buf *buf)
{
        int v;
        v = buf->buf_p - buf->buf;
        
        char *nbuf = realloc(buf->buf, buf->buf_size + BUF_SIZE);
        if (nbuf == NULL) {
                log_write("Failed to resize\n");
                return -1;
        }

        buf->buf_size += BUF_SIZE;
        buf->buf_p = nbuf + v;
        buf->buf = nbuf;
        return 0;
}

void clear_buf(client_buf *buf)
{
        if (buf) {
                free(buf->buf);
                free(buf);
        }
}

char *readline_buf(client_buf *buf)
{
        char *strptr;
        strptr = buf->buf_p;

        while (buf->c <= buf->buf_length) {
                if (*buf->buf_p != '\n') {
                        buf->buf_p++;
                        buf->c++;
                }       
                else {
                        *buf->buf_p = '\0';
                        buf->buf_p++;
                        buf->c++;
                        return strptr;
                }
        }
        return NULL;
}
        
int init_cstate(client_state *client)
{
	memset(&client->cli_addr, '\0', sizeof(client->cli_addr));
	client->port = 0;
	client->cgicheck = 0;
	client->httpscheck = 0;
    client_buf *readbuf = init_buf();
    if (readbuf == NULL)
	{
        log_write("Out of memory\n");
    	return -1;
    }

    client->readbuf = readbuf;
    client->writebuf = NULL;
    return 0;
}

void clear_cstate(client_state *client)
{               
        clear_buf(client->readbuf);
        clear_buf(client->writebuf);        

        client->readbuf = NULL;
        client->writebuf = NULL;
}

void reset_cstate(client_state *client)
{
        clear_cstate(client);
        init_cstate(client);
}

