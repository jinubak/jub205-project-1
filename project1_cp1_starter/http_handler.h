#ifndef HTTP_HANDLER_H_
#define HTTP_HANDLER_H_

#define GET	0
#define POST 1
#define HEAD 2
#define UNKNOWN -1
#define KEEPALIVE 0
#define CLOSE_CON 1
#define TEXT_HTML 0
#define TEXT_CSS	1
#define IMAGE_PNG 2
#define IMAGE_JPEG 3
#define IMAGE_GIF 4

#define MAX_LINE 4096

#include "client_handler.h"

int http_parser(int, client_state *, void *);

#endif
