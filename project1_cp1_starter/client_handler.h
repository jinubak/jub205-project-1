#ifndef CLIENT_HANDLER_H_
#define CLIENT_HANDLER_H_


#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <openssl/ssl.h>
#include <netinet/in.h>

#define BUF_SIZE 4092

typedef struct client_buf {
        int buf_length;
        int buf_size;
        char *buf_p;
        char *buf;
        int c;
} client_buf;


typedef struct {
        int method;
        char *uri;
        char *v;
        int isclose;
        struct stat statf;
        int content_length;
        FILE *f;
        int m_type;
        client_buf *readbuf;
        client_buf *writebuf;
		SSL* ssl_info;
		struct sockaddr_in cli_addr;
		int port;
		int httpscheck;
		int cgicheck;
} client_state;

client_buf *init_buf();
int resize_buf(client_buf *);
void clear_buf(client_buf *);
char *readline_buf(client_buf *);
int init_cstate(client_state *);
void reset_cstate(client_state *);
void clear_cstate(client_state *);

#endif

