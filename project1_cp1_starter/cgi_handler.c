#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "cgi_handler.h"
#include "lisod_helper.h"
#include "http_handler.h"
#include "logger.h"

int envbuilder(char *envp[], client_state *client, l_engine *l_e);
char *str_build(char *);
int get_body(client_state *client);

/// Referred to example code from CMU ///
void execve_error_handler()
{
    switch (errno)
    {
        case E2BIG:
            fprintf(stderr, "The total number of bytes in the environment \
(envp) and argument list (argv) is too large.\n");
            return;
        case EACCES:
            fprintf(stderr, "Execute permission is denied for the file or a \
script or ELF interpreter.\n");
            return;
        case EFAULT:
            fprintf(stderr, "filename points outside your accessible address \
space.\n");
            return;
        case EINVAL:
            fprintf(stderr, "An ELF executable had more than one PT_INTERP \
segment (i.e., tried to name more than one \
interpreter).\n");
            return;
        case EIO:
            fprintf(stderr, "An I/O error occurred.\n");
            return;
        case EISDIR:
            fprintf(stderr, "An ELF interpreter was a directory.\n");
            return;
        case ELIBBAD:
            fprintf(stderr, "An ELF interpreter was not in a recognised \
format.\n");
            return;
        case ELOOP:
            fprintf(stderr, "Too many symbolic links were encountered in \
resolving filename or the name of a script \
or ELF interpreter.\n");
            return;
        case EMFILE:
            fprintf(stderr, "The process has the maximum number of files \
open.\n");
            return;
        case ENAMETOOLONG:
            fprintf(stderr, "filename is too long.\n");
            return;
        case ENFILE:
            fprintf(stderr, "The system limit on the total number of open \
files has been reached.\n");
            return;
        case ENOENT:
            fprintf(stderr, "The file filename or a script or ELF interpreter \
does not exist, or a shared library needed for \
file or interpreter cannot be found.\n");
            return;
        case ENOEXEC:
            fprintf(stderr, "An executable is not in a recognised format, is \
for the wrong architecture, or has some other \
format error that means it cannot be \
executed.\n");
            return;
        case ENOMEM:
            fprintf(stderr, "Insufficient kernel memory was available.\n");
            return;
        case ENOTDIR:
            fprintf(stderr, "A component of the path prefix of filename or a \
script or ELF interpreter is not a directory.\n");
            return;
        case EPERM:
            fprintf(stderr, "The file system is mounted nosuid, the user is \
not the superuser, and the file has an SUID or \
SGID bit set.\n");
            return;
        case ETXTBSY:
            fprintf(stderr, "Executable was open for writing by one or more \
processes.\n");
            return;
        default:
            fprintf(stderr, "Unkown error occurred with execve().\n");
            return;
    }
}
/// From the example code (CMU course website)

int cgi_helper(int connfd, client_state *client, void *self)
{
    l_engine *l_e = (l_engine *) self;
    pid_t pid;
    int stdin_p[2];
    int stdout_p[2];
    
	// Piping!
    if (pipe(stdin_p) < 0)
    {
        fprintf(stderr, "stdin piping fail.\n");
        return -1;
    }

    if (pipe(stdout_p) < 0)
    {
        fprintf(stderr, "stdout piping fail.\n");
        return -1;
    }
    //Forking! 
    pid = fork();
   
    if (pid < 0)
    {
        fprintf(stderr, "Fail forking\n");
        exit(EXIT_FAILURE);
    }

    
    if (pid == 0)
    {
                char *ARGV[2];
                char *ENVP[30];
                
                ARGV[0] = l_e->cgif;
                ARGV[1] = NULL;
        
                envbuilder(ENVP, client, l_e);
        
        close(stdout_p[0]);
        close(stdin_p[1]);
        dup2(stdout_p[1], fileno(stdout));
        dup2(stdin_p[0], fileno(stdin));
        
        if (execve(l_e->cgif, ARGV, ENVP))
        {
            execve_error_handler();
            fprintf(stderr, "execve syscall error.\n");
            exit(EXIT_FAILURE);
        }
        
    }

    if (pid > 0)
    {
        close(stdout_p[1]);
        close(stdin_p[0]);

                if (client->method == POST) {
                        if (get_body(client))
                                return -1;

                        if (write(stdin_p[1], client->readbuf->buf_p,client->content_length) < 0) {
                                fprintf(stderr, "Error.\n");
                                return -1;
                        }
                }

        close(stdin_p[1]); 
                pipesocket(stdout_p[0], connfd);
                return 0;
   }
        fprintf(stderr, "Damn it! How did you get here?!");
        return -1;
}

int envbuilder(char *envp[], client_state *client, l_engine *l_e)
{
        int ind = 0;
        char buf[1024];
        char *ptr, *line;
        line = "";

        envp[ind++] = str_build("GATEWAY_INTERFACE=CGI/1.1");
        envp[ind++] = str_build("SERVER_PROTOCOL=HTTP/1.1");
        envp[ind++] = str_build("SERVER_SOFTWARE=Liso/1.0");
        sprintf(buf, "SCRIPT_NAME=%s", l_e->cgif);
        envp[ind++] = str_build(buf);
        sprintf(buf, "REMOTE_ADDR=%s", inet_ntoa(client->cli_addr.sin_addr));
        envp[ind++] = str_build(buf);
        sprintf(buf, "SERVER_PORT=%d", client->port);
        envp[ind++] = str_build(buf);
        sprintf(buf, "REQUEST_URI=%s", client->uri);
        envp[ind++] = str_build(buf);
        sprintf(buf, "PATH_INFO=%s", client->uri+strlen(C_PATH)-1);
        envp[ind++] = str_build(buf);
        
        switch (client->method) {
                case GET:
                        envp[ind++] = str_build("REQUEST_METHOD=GET");
                        break;
                case POST:
                        envp[ind++] = str_build("REQUEST_METHOD=POST");
                        break;
                case HEAD:
                        envp[ind++] = str_build("REQUEST_METHOD=HEAD");
                        break;
        }
        
        if ((ptr = strchr(client->uri, '?')) != NULL) {
                sprintf(buf, "QUERY_STRING=%s", ptr+1);
                envp[ind++] = str_build(buf);
        }
        
        while (strcmp(line, "\r")) {
                if((line = readline_buf(client->readbuf)) == NULL) {
                        log_write("Bad Request format.\n");
                        return -1;
                }
                else if (!strncasecmp(line, "Content-Length", strlen("Content-Length"))) {
                        sprintf(buf, "CONTENT_LENGTH=%s", line+strlen("Content-Length")+2);
                        envp[ind++] = str_build(buf);
                }
                else if (!strncasecmp(line, "Content-Type", strlen("Content-Type"))) {
                        sprintf(buf, "CONTENT_TYPE=%s", line+strlen("Content-Type")+2);
                        envp[ind++] = str_build(buf);
                }
                else if (!strncasecmp(line, "Accept", strlen("Accept"))) {
                        sprintf(buf, "HTTP_ACCEPT=%s", line+strlen("Accept")+2);
                        envp[ind++] = str_build(buf);
                }
                else if (!strncasecmp(line, "Referer", strlen("Referer"))) {
                        sprintf(buf, "HTTP_REFERER=%s", line+strlen("Referer")+2);
                        envp[ind++] = str_build(buf);
                }
                else if (!strncasecmp(line, "Accept-Encoding", strlen("Accept-Encoding"))) {
                        sprintf(buf, "HTTP_ACCEPT_ENCODING=%s", line+strlen("Accept-Encoding")+2);
                        envp[ind++] = str_build(buf);
                }
                else if (!strncasecmp(line, "Accept-Language", strlen("Accept-Language"))) {
                        sprintf(buf, "HTTP_ACCEPT_LANGUAGE=%s", line+strlen("Accept-Language")+2);
                        envp[ind++] = str_build(buf);
                }
                else if (!strncasecmp(line, "Accept-Charset", strlen("Accept-Charset"))) {
                        sprintf(buf, "HTTP_ACCEPT_CHARSET=%s", line+strlen("Accept-Charset")+2);
                        envp[ind++] = str_build(buf);
                }
                else if (!strncasecmp(line, "Cookie", strlen("Cookie"))) {
                        sprintf(buf, "HTTP_COOKIE=%s", line+strlen("Cookie")+2);
                        envp[ind++] = str_build(buf);
                }
                else if (!strncasecmp(line, "User-Agent", strlen("User-Agent"))) {
                        sprintf(buf, "HTTP_USER_AGENT=%s", line+strlen("User-Agent")+2);
                        envp[ind++] = str_build(buf);
                }
                else if (!strncasecmp(line, "Connection", strlen("Connection"))) {
                        sprintf(buf, "HTTP_CONNECTION=%s", line+strlen("Connection")+2);
                        envp[ind++] = str_build(buf);
                }
                else if (!strncasecmp(line, "Host", strlen("Host"))) {
                        sprintf(buf, "HTTP_HOST=%s", line+strlen("Host")+2);
                        envp[ind++] = str_build(buf);
                }
        }
        envp[ind++] = NULL;
        return 0;
}

char *str_build(char *str)
{
        int len = strlen(str) + 1;
        char *newstr = (char *) malloc(len);
        if (newstr == NULL)
                return NULL;
        else {
                strcpy(newstr, str);
                return newstr;
        }
}

int get_body(client_state *client)
{
        char *line = "";
        client->content_length = -1;

        while (strcmp(line, "\r")) {
                if((line = readline_buf(client->readbuf)) == NULL) {
                        log_write("Bad Request format.\n");
                        return -1;
                }
                if (!strncasecmp(line, "Content-Length", strlen("Content-Length"))) {
                        client->content_length = atoi(line+strlen("Content-Length: "));
                }
        }

        if (client->content_length == -1)
                return -1;
        else
                return 0;
}
