#ifndef LOGGER_H_
#define LOGGER_H_

void init_log(char *);
void log_write(char *, ...);
void close_log();

#endif
