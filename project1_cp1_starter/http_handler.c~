#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "http_handler.h"
#include "lisod_helper.h"
#include "logger.h"
#include "cgi_handler.h"
static int statline_handler(char *line, client_state *client);
static int GET_handler(client_state *client, l_engine *engine);
static int POST_handler(client_state *client, l_engine *engine);
static int HEAD_handler( client_state *client, l_engine *engine);
static int open_file(client_state *client, l_engine *l_e);
static void get_type(char *path, client_state *client);
static int error_handler(client_state *client, char *s, char *msg);
static int statline_add(client_state *client, char *s, char *msg);
static int buf_add(client_state *client, char *buf, int len);
static int headerbuilder(client_state *client);
static int bodybuilder(client_state *client);
static void bodyerror_handler(char *buf, char *s, char *msg);
static int is_cgi(char *uri);


int http_parser(int connfd, client_state *client, void *self)
{
        char *line;
        l_engine *l_e = (l_engine *) self;
        
        if((line = readline_buf(client->readbuf))==NULL)
		{
			log_write("400 Bad Request\n");
			return error_handler(client, "400", "Bad Request");
		}
   
   		if (strcmp(client->v, "HTTP/1.1"))
		{
			log_write("505 HTTP Version not supported\n");
            return error_handler(client, "505", "HTTP Version not supported");
        }
        
        if (statline_handler(line,client))
        {
        	log_write("400 Bad Request\n");
            return error_handler(client, "400", "Bad Request");
        }

		if (is_cgi(client->uri))
		{
			if (cgi_helper(connfd, client, l_e)) 
			{
				log_write("500 Internal Server Error \n");
				return error_handler(client, "500", "Internal Server Error");
			}
			return 1;
			
		}
        switch (client->method)
		{
                case GET:
                        return GET_handler( client, l_e);
                case POST:
						return POST_handler( client, l_e);
                case HEAD:
                        return HEAD_handler( client, l_e);
                case UNKNOWN:
                		log_write("501 Not Implemented\n");
                        return error_handler(client, "501", "Not Implemented");
        }
	return 0;
}


static int statline_handler(char *line, client_state *client)
{
        char *p; 
		char *method;
		char *uri;
		char *v;
        method = line;
        if ((p = strchr(line, ' ')) == NULL)
                return -1;

        *p = '\0';
        if (strcmp(method, "GET") == 0) 
                client->method = GET;
        else if (strcmp(method, "POST") == 0) 
                client->method = POST;
        else if (strcmp(method, "HEAD") == 0)
                client->method = HEAD;
        else 
                client->method = UNKNOWN;

        uri = ++p;
        if ((p = strchr(uri, ' ')) == NULL)
                return -1;

        *p = '\0';
        client->uri = uri;
        
        v = ++p;
        if ((p = strchr(v, '\r')) == NULL)
                return -1;

        *p = '\0';
        client->v = v;
        return 0;
}

static int GET_handler(client_state *client, l_engine *l_e)
{
    char *line;
    while (strcmp(line, "\r"))
	{
    	line = readline_buf(client->readbuf);
        if (!strncasecmp(line, "CONNECTION", strlen("CONNECTION"))) 
		{
			if (!strncasecmp(line+strlen("CONNECTION: "), "CLOSE",strlen("CLOSE")))
			{
				client->isclose = CLOSE_CON;
			}
			else
			{
				client->isclose = KEEPALIVE;
			}
		}
				
	}
	
	if (open_file(client, l_e))
	{
		log_write("404 NOt Found\n");
		return error_handler(client, "404", "Not Found");
	}
	
	if(headerbuilder(client))
	{
		log_write("Header building failed\n");
		fclose(client->f);
		return -1;
	}
	
	if (bodybuilder(client)) 
	{
		log_write("Body building failed\n");
		fclose(client->f);
		return -1;
	}
	fclose(client->f);
	return 0;
}

static int POST_handler(client_state *client, l_engine *l_e)
{
	char *line;
	client->content_length = -1;
    while (strcmp(line, "\r")) 
	{
        if((line = readline_buf(client->readbuf)) == NULL)
		{
        	log_write("400 Bad Request\n");
            return error_handler(client, "400", "Bad Request");
		}
        else if (!strncasecmp(line, "CONNECTION", strlen("CONNECTION")))
		{
            if (!strncasecmp(line+strlen("CONNECTION: "), "CLOSE", strlen("CLOSE")))
			{
                client->isclose = CLOSE_CON; 
            }
            else
            {
                client->isclose = KEEPALIVE;
            }
        }
        else if (!strncasecmp(line, "Content-Length", strlen("Content-Length")))
		{
            client->content_length = atoi(line+strlen("Content-Length: "));
        }
    }
    if (client->content_length == -1)
    	log_write("411 Length Required\n");
        return error_handler(client, "411", "Length Required");
    if (statline_add(client, "200", "OK"))
        return -1;
	if (buf_add(client, "\r\n", strlen("\r\n")))
        return -1;
    return 0;
}

static int HEAD_handler(client_state *client, l_engine *l_e)
{
	char *line;
    while (strcmp(line, "\r"))
	{
    	if((line = readline_buf(client->readbuf)) == NULL)
   		{
			log_write("400 Bad Request\n");
            return error_handler(client, "400", "Bad Request");
		}
        else if (!strncasecmp(line, "CONNECTION", strlen("CONNECTION")))
		{
            if (!strncasecmp(line+strlen("CONNECTION: "), "CLOSE",strlen("CLOSE")))
			{
                client->isclose = CLOSE_CON; 
            }
            else
                client->isclose = KEEPALIVE;
        }
    }
                
    if (open_file(client, l_e))
	{
		log_write("404 Not Found\n");
        return error_handler(client, "404", "Not Found");
    }
        
    if(headerbuilder(client))
	{
        fclose(client->f);
        return -1;
    }
	return 0;
}


static int open_file(client_state *client, l_engine *l_e)
{
        char path[MAX_LINE];
        // create path
        strcpy(path, l_e->wwwf);
        if (!strcmp(client->uri, "/")) 
                strcat(path, "index.html");
        else 
                strcat(path, client->uri);

        if ((client->f = fopen(path, "r")) == NULL) {
                return -1;
        }       

        if (stat(path, &client->statf))
		{
			fclose(client->f);
            return -1;
        }

        get_type(path, client);
        return 0;
}

static void get_type(char *path, client_state *client)
{
        char *type;
        type = strchr(path, '.');

        if (type == NULL)
                client->m_type = UNKNOWN;
        else
		{
                type++;
                if (!strcmp(type, "html"))
                        client->m_type = TEXT_HTML;
                else if (!strcmp(type, "css"))
                        client->m_type = TEXT_CSS;
                else if (!strcmp(type, "png"))
                        client->m_type = IMAGE_PNG;
                else if (!strcmp(type, "jpeg"))
                        client->m_type = IMAGE_JPEG;
                else if (!strcmp(type, "gif"))
                        client->m_type = IMAGE_GIF;
                else
                        client->m_type = UNKNOWN;
        }
}

static int error_handler(client_state *client, char *s, char *msg)
{
        char buf[BUF_SIZE];
        char buf2[BUF_SIZE];
        time_t rt;
        bodyerror_handler(buf2, s, msg);
        /* add status line */
        if (statline_add(client, s, msg))
                return -1;

        rt = time(NULL);
        strftime(buf, sizeof(buf), "Date: %a, %d %b %Y %H:%M:%S GMT\r\n",
                        gmtime(&rt));
        
        sprintf(buf, "%sServer: Liso\r\n", buf);
        sprintf(buf,"%sContent-Type: text/html\r\n", buf);
		sprintf(buf, "%sContent-Length: %d\r\n", buf, (int)strlen(buf2));
        sprintf(buf, "%s\r\n", buf);
        sprintf(buf, "%s%s", buf, buf2);
        
        return buf_add(client, buf, strlen(buf));
}


static void bodyerror_handler(char *buf, char *s, char *msg)
	{
		sprintf(buf, "<html><title>Liso Server Error</title>");
		sprintf(buf, "%s<body bgcolor=""ffffff"">\r\n", buf);
		sprintf(buf, "%s%s: %s\r\n", buf, s, msg);
		sprintf(buf, "%s<hr><em>The Liso server</em>\r\n", buf);
		sprintf(buf, "%s</body></html>", buf);
	}
static int buf_add(client_state *client, char *buf, int len)
{
        /* if sendbuf == NULL, allocate new buffer */
        if (client->writebuf == NULL) {
                if ((client->writebuf = init_buf()) == NULL)
                        return -1;
        }
        
        /* if there is no enough space in sendbuf, extend sendbuf */
        if ((client->writebuf->buf_size - client->writebuf->buf_length)< len)
		{
                if(resize_buf(client->writebuf))
                        return -1;
        }
        
        memcpy(client->writebuf->buf_p, buf, len);
        client->writebuf->buf_length += len;
        client->writebuf->buf_p += len;
        return 0;
}

static int statline_add(client_state *client, char *s, char *msg)
{
        char buf[MAX_LINE];
        sprintf(buf, "HTTP/1.1 %s %s\r\n", s, msg);
        
        return buf_add(client, buf, strlen(buf));
}

static int headerbuilder(client_state *client)
{
        char buf[BUF_SIZE], mtime[100];
        time_t rt;
        /* add status line */
        if(statline_add(client, "200", "OK"))
                return -1;

        rt = time(NULL);
        strftime(buf, sizeof(buf), "Date: %a, %d %b %Y %H:%M:%S GMT\r\n",
                        gmtime(&rt));
        
        sprintf(buf, "%sServer: Liso\r\n", buf);
        
        if (client->isclose) 
                sprintf(buf, "%sConnection: Close\r\n", buf);
        else
                sprintf(buf, "%sConnection: keepAlive\r\n", buf);

        sprintf(buf, "%sContent-Length: %lld\r\n", buf, (long long)client->statf.st_size);
        
        switch (client->m_type) {
                case TEXT_HTML:
                        sprintf(buf, "%sContent-Type: text/html\r\n", buf);
                        break;
                case TEXT_CSS:
                        sprintf(buf, "%sContent-Type: text/css\r\n", buf);
                        break;
                case IMAGE_PNG:
                        sprintf(buf, "%sContent-Type: image/png\r\n", buf);
                        break;
                case IMAGE_JPEG:
                        sprintf(buf, "%sContent-Type: image/jpeg\r\n", buf);
                        break;
                case IMAGE_GIF:
                        sprintf(buf, "%sContent-Type: image/gif\r\n", buf);
                        break;
        }
        strftime(mtime, sizeof(mtime), "%a, %d %b %Y %H:%M:%S GMT",
                         gmtime(&client->statf.st_mtime));
        sprintf(buf, "%sLast-Modified: %s\r\n", buf, mtime);
        sprintf(buf, "%s\r\n", buf);

        return buf_add(client, buf, strlen(buf));
}

static int bodybuilder(client_state *client)
{
        long long body;
        char buf[BUF_SIZE];

        body = client->statf.st_size;
        while (body) {
                if (body < BUF_SIZE) {
                        fread(buf, sizeof(char), body, client->f);
                        if (buf_add(client, buf, body))
                                return -1;
                        body -= body;
                }
                else {
                        fread(buf, sizeof(char), BUF_SIZE, client->f);
                        if (buf_add(client, buf, BUF_SIZE))
                                return -1;
                        body -= BUF_SIZE;
                }
        }
        return 0;
}

static int is_cgi(char *uri)
{
	if (!strncmp(uri, C_PATH, strlen(C_PATH)))
			return 1;
		else
			return 0;
}

