#ifndef LISOD_HELPER_H_
#define LISOD_HELPER_H_
#include <sys/select.h>

#include "client_handler.h"

#define MAX_CLIENT 1024
#define MAX_REQUEST 10000   
#define R_SUCCESS 1
#define R_EOF 0
#define R_FAIL -1
#define S_SUCCESS 0
#define S_FAIL -1

typedef int(* http_handler)(int, client_state *, void *);

typedef struct {
        int port;
        int httpsport;
        char *logfile;
        char *lockfile;
        char *wwwf;
        char *cgif;
        char *password;
        char *cert;
        http_handler h;
} l_engine;

typedef struct {
        int maxfd;
        fd_set read_set;
        fd_set s_set;
        fd_set p_set;
        fd_set w_set;
        fd_set ready_set;
        int nready;
        client_state clients[FD_SETSIZE];
		SSL_CTX *ssl_info;
} client_pool;


void init_server(l_engine *, int, int, char *, char *, char*,
                char *, char *, char * ,http_handler);
int start_server(l_engine *);
void close_server(int);
void pipesocket(int, int);
#endif
