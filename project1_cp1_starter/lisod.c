#include <stdio.h>
#include <stdlib.h>

#include "lisod_helper.h"
#include "http_handler.h"
#include "daemon.h"
#include "logger.h"
#define SHOWUSAGE "%s <HTTP port> <HTTPS port> <log> <lock> <www> <cgi> <key> <cert>\n"

int main(int argc, char *argv[])
{
        int port;
		int porthttps;
        char *logfile;
		char *lockfile;
		char *wwwf;
		char *cgif;
		char *key;
		char *cert;
        if (argc < 9) {
                printf(SHOWUSAGE, argv[0]);
                return EXIT_FAILURE;
        }
        port = atoi(argv[1]);
        porthttps = atoi(argv[2]);
        logfile = argv[3];
        lockfile = argv[4];
        wwwf = argv[5];
        cgif = argv[6];
        key = argv[7];
        cert = argv[8];
        
        daemonize(lockfile);
        
        l_engine l_e;
        init_log(logfile);
        init_server(&l_e, port, porthttps, logfile, lockfile, wwwf, cgif, key, cert, http_parser);
        log_write("Setting up Liso server on port %d and https port %d\n",port, porthttps);
        return start_server(&l_e);
}
