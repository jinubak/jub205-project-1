Networks and Distributed Systems
Project 1: A Web Server Called Liso
Name: Jin U Bak

In this project, we are asked to build a server that supports
HTTP/1.1, HTTPS and CGI.

This is mainly divided into three parts:

1) concurrency connection using select()
 - In this part, we had to develop on a given server code that does not support
   multiple clients. In order to implement, I have used select() so that it can
   support multiple clients at a time. In order to implement, I have gone through
   some online example codes and also some good pdf files provided by CMU.

2) HTTP 1.1 parser that supports HEAD, GET, POST
 - In this part, we were to implement HTTP parser that can parse HEAD, GET and POST.
   First, I looked through RCF 2616 for design purpose. Also used some materials from
   CMU that summaried all the required features and some of recitation materials from
   CMU course website.

3) HTTPS and CGI
 - This was the hardest part as it required a lot of understanding. Luckily, I was able to
   find some example codes for HTTPS and CGI that were provided by CMU Network course.
   Example codes were really useful and helpful as it was quite usable. Also for design purpose,
   I went through RFC 2818 and 3875.

For usage, ./lisod <HTTP port> <HTTPS port> <log> <lock> <www> <cgi> <key> <cert>
