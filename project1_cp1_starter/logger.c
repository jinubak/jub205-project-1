#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include "logger.h"

static FILE *f;   

void init_log(char *file)
{
    if ((f = fopen(file, "w")) == NULL) 
	{
        fprintf(stderr, "Log Initilization Fail.\n");
        exit(EXIT_FAILURE);
    }
}
void log_write(char *msg, ...)
{
    va_list ap;
    va_start(ap, msg);

    char timeholder[100]; // For time holder
    time_t rt;
    rt = time(NULL);
    strftime(timeholder, sizeof(timeholder), "%x %H:%M:%S", localtime(&rt)); // Save time
    fprintf(f, "%s: ", timeholder);
    vfprintf(f, msg, ap);
}
void close_log()
{
    if (fclose(f) != 0) 
	{
        fprintf(stderr, "Log Close Fail.\n");
        exit(EXIT_FAILURE);
    }
}
