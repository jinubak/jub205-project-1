#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <arpa/inet.h>
#include "lisod_helper.h"
#include "logger.h"

int open_socket(int port);
void init_pool(client_pool *p);
int add_client(int connfd);
int ssl_client(int connfd);
int ssl_read_helper(int connfd);
int ssl_write_helper(int connfd);
void select_handler(int fd, l_engine *l_e);
int read_helper(int connfd);
int write_helper(int connfd);
int pipe_read(int connfd);

client_pool pool;    
int p_s[FD_SETSIZE] = {-1};

void init_server(l_engine *l_e, int port,int httpsport, char *log,char *lock,
                char *www,char *cgi,char *key,char *cert, http_handler h)
{
        l_e->port = port;
        l_e->httpsport = httpsport;
        l_e->logfile = log;
        l_e->lockfile = lock;
        l_e->wwwf = www;
        l_e->cgif = cgi;
        l_e->password = key;
        l_e->cert = cert;
        l_e->h = h;
        
        init_pool(&pool); // Initialize client_pool
        
        // Initialize SSL // HELP FROM CMU's COURSE WEBSITE EXAMPLE CODE !!
		SSL_load_error_strings();
		SSL_library_init();
		 
		 /* we want to use TLSv1 only */
		if ((pool.ssl_info = SSL_CTX_new(TLSv1_server_method())) == NULL)
		{
			log_write("Error creating SSL context\n");
			exit(EXIT_FAILURE);
		}
	
		if (SSL_CTX_use_PrivateKey_file(pool.ssl_info, l_e->password,SSL_FILETYPE_PEM) == 0) 
		{
			SSL_CTX_free(pool.ssl_info);
			log_write("Error associating private key\n");
			exit(EXIT_FAILURE);
		}
	
		if (SSL_CTX_use_certificate_file(pool.ssl_info, l_e->cert,SSL_FILETYPE_PEM) == 0)
		{
			SSL_CTX_free(pool.ssl_info);
			log_write("Error associating certificate\n");
			exit(EXIT_FAILURE);
		}
		
}

void set_server(l_engine* l_e, http_handler h)
{
        l_e->h = h;
}

int start_server(l_engine *l_e)
{
        int connfd, connfd_ssl, cli_fd;
        int i;
        int flag;

        if ((connfd = open_socket(l_e->port)) < 0) {
        		SSL_CTX_free(pool.ssl_info);
                log_write("Failed opening listen socket\n");
                return EXIT_FAILURE;
        }
		
		if ((connfd_ssl = open_socket(l_e->httpsport)) < 0) {
			SSL_CTX_free(pool.ssl_info);
			log_write("Failed opening ssl listen socket\n");
			return EXIT_FAILURE;
		}
		
		
        // Put connfd and connfd_ssl into the pool!
        pool.maxfd = connfd > connfd_ssl ? connfd : connfd_ssl;
        FD_SET(connfd, &pool.read_set);
		FD_SET(connfd_ssl, &pool.read_set);
        log_write("Server Listening...\n");

        
        while (1) {
                pool.ready_set = pool.read_set;
                pool.nready = select(pool.maxfd+1, &pool.ready_set, &pool.w_set, NULL, NULL);

                if (pool.nready == -1) {
                        if (errno == EINTR) 
                                continue;
                        else {
                                close(connfd);
                                log_write("selecting error\n");
                                return EXIT_FAILURE;
                        }
                }

                if (FD_ISSET(connfd, &pool.ready_set))
				{
                        if ((cli_fd = add_client(connfd)) == -1)
                                return EXIT_FAILURE;
                        pool.clients[cli_fd].port = l_e->port;
                }               
                
                
                if (FD_ISSET(connfd_ssl, &pool.ready_set))
				{
					if ((cli_fd = ssl_client(connfd_ssl)) == -1)
						return -1;
					else if (cli_fd == -2)
						continue;
					else 
					{
						flag = fcntl(cli_fd, F_GETFL, 0);
						fcntl(cli_fd, F_SETFL, flag | O_NONBLOCK); // Non blocking
						pool.clients[cli_fd].port = l_e->httpsport;
					}
				}
                
                for (i = 0; (i <= pool.maxfd) && (pool.nready > 0); i++) {
                        if (i == connfd || i == connfd_ssl)
                                continue;
                        else if (FD_ISSET(i, &pool.ready_set)) 
						{
                                select_handler(i, l_e);
                        }
                        else if (FD_ISSET(i, &pool.w_set))
						{
								if(FD_ISSET(i, &pool.s_set))
								{
									ssl_write_helper(i);
								}
								else
								{
									write_helper(i);
								}
                                FD_CLR(i, &pool.w_set);
                                reset_cstate(&pool.clients[i]);
                        }
                }
        }
        return EXIT_SUCCESS;
}

void pipesocket(int pfd, int sfd)
{
	int f;
	p_s[pfd] = sfd;
	FD_SET(pfd, &pool.p_set);
	FD_SET(pfd, &pool.read_set);
	f = fcntl(pfd, F_GETFL, 0);
	fcntl(pfd, F_SETFL, f | O_NONBLOCK); // Non blocking
	if (pool.maxfd < pfd)
		pool.maxfd = pfd;
}

void init_pool(client_pool *p)
{
        p->maxfd = -1;
        FD_ZERO(&p->read_set);
        FD_ZERO(&p->s_set);
        FD_ZERO(&p->p_set);
        FD_ZERO(&p->w_set);
        FD_ZERO(&p->ready_set);
        p->nready = 0;
}


int open_socket(int port)
{
        int serverfd, optval = 1;
        struct sockaddr_in addr;

        /* create a socket descriptor */
        if ((serverfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
                return -1;

        /* eliminates "Address already in use" error from bind */
        if (setsockopt(serverfd, SOL_SOCKET, SO_REUSEADDR,
                                (const void *) &optval, sizeof(int)) < 0)
                return -1;

        /* bind IP address and port to listenfd */
        bzero((char *) &addr, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = htonl(INADDR_ANY);
        addr.sin_port = htons((unsigned short) port);
        if (bind(serverfd, (struct sockaddr *) &addr, sizeof(addr)) < 0)
                return -1;

        if (listen(serverfd, MAX_CLIENT) < 0)
                return -1;

        return serverfd;
}


int add_client(int connfd)
{
        int cli_fd;
        socklen_t c_len = sizeof(struct sockaddr_in);
        struct sockaddr_in cli_addr;

        // accepting new client
        if ((cli_fd = accept(connfd, (struct sockaddr *) &cli_addr,
                                        &c_len)) < 0) {
                close(connfd);
                log_write("Error accepting connection\n");
                return -1;
        }

        if (init_cstate(&pool.clients[cli_fd])) 
		{    
            close(cli_fd);
        }
        else 
		{
			memcpy(&pool.clients[cli_fd].cli_addr, &cli_addr, sizeof(cli_addr));
            FD_SET(cli_fd, &pool.read_set);
            if (pool.maxfd < cli_fd)
                pool.maxfd = cli_fd;
        }

        return cli_fd;
}


void select_handler(int connfd, l_engine *l_e)
{
	int ret;
	if(FD_ISSET(connfd, &pool.p_set))
		ret = pipe_read(connfd);
	else if(FD_ISSET(connfd, &pool.s_set))
	{
		ret = ssl_read_helper(connfd);
	}
	else
	{
		ret = read_helper(connfd);
	}
    switch (ret) 
	{
        case R_SUCCESS:
        {
        	ret = l_e->h(connfd, &pool.clients[connfd], l_e);
            if (ret<0)
			{
				if(FD_ISSET(connfd,&pool.s_set))
					FD_CLR(connfd,&pool.s_set);
				FD_CLR(connfd, &pool.read_set);
				clear_cstate(&pool.clients[connfd]);
				close(connfd);
			}
			else if (ret==0)
			{
				FD_SET(connfd, &pool.w_set);
			}
            break;
		}
                        
        case R_EOF:
        {
        	if (FD_ISSET(connfd, &pool.p_set))
			{
				FD_SET(p_s[connfd], &pool.w_set);
				FD_CLR(connfd, &pool.p_set);
				FD_CLR(connfd, &pool.read_set);
				close(connfd);
				break;
			}
        }	
        case R_FAIL:
        {
        	if (FD_ISSET(connfd, &pool.p_set))
			{
				FD_CLR(connfd, &pool.p_set);
				FD_CLR(connfd, &pool.read_set);
				close(connfd);
				connfd = p_s[connfd];
			}	
        	
        	if(FD_ISSET(connfd,&pool.s_set))
				FD_CLR(connfd,&pool.s_set);
            FD_CLR(connfd, &pool.read_set);
            clear_cstate(&pool.clients[connfd]);
            close(connfd);
            break;
        }
    }
}

int read_helper(int connfd)
{
        int ret;
        char buf[BUF_SIZE];
        client_buf *c_buf; 

        c_buf = pool.clients[connfd].readbuf;
        
        while ((ret = recv(connfd, buf, BUF_SIZE, MSG_DONTWAIT)) != 0) {
                if (ret < 0) {
                        if (errno == EINTR)
                                continue;
                        else if (errno == EAGAIN)
                                return R_SUCCESS;
                        else {
                                log_write("Failed receiving data from client %d. errno %d\n", connfd, errno);
                                return R_FAIL;
                        }
                }
                else {
                        if (c_buf->buf_length + ret > MAX_REQUEST) {
                                log_write("Request is too long\n");
                                return R_FAIL;
                        }
                        else {
                                if ((c_buf->buf_length + ret) > c_buf->buf_size) {
                                        if (resize_buf(c_buf)) {
                                                log_write("Out of memory\n");
                                                return R_FAIL;
                                        }
                                }
                                memcpy(c_buf->buf + c_buf->buf_length, buf, ret);
                                c_buf->buf_length += ret;
                        }
                }
        }

        return R_EOF;
}

int write_helper(int connfd)
{
        int w_length;
        client_buf *cbuf;
        cbuf = pool.clients[connfd].writebuf;
        
        if ((w_length = send(connfd, cbuf->buf, cbuf->buf_length, MSG_DONTWAIT)) != cbuf->buf_length) {
                log_write("send fail\n");                     
                return S_FAIL;
        }

        return S_SUCCESS;
}

void close_server(int ret)
{
        exit(ret);
}


/// SSL Handler
int ssl_client(int connfd)  // New SSL client
{
		int cli_fd;
	
		if ((cli_fd = add_client(connfd)) == -1)
			return -1;
		if ((pool.clients[cli_fd].ssl_info = SSL_new(pool.ssl_info)) == NULL)
		{
			close(connfd);
	        SSL_CTX_free(pool.ssl_info);
			return -1;
		}
	
		if (SSL_set_fd(pool.clients[cli_fd].ssl_info, cli_fd) == 0) 
		{
			close(connfd);
			SSL_free(pool.clients[cli_fd].ssl_info);
	        SSL_CTX_free(pool.ssl_info);
			return -1;
		}
	
	    if (SSL_accept(pool.clients[cli_fd].ssl_info) <= 0) 
		{
			FD_CLR(cli_fd, &pool.read_set);
			close(connfd);
			SSL_shutdown(pool.clients[cli_fd].ssl_info);
			SSL_free(pool.clients[cli_fd].ssl_info);
			return -2;
		}
		pool.clients[cli_fd].httpscheck = 1;
		FD_SET(cli_fd, &pool.s_set);
		return cli_fd;
}

int ssl_read_helper(int connfd)
{
	int ret;
	char buf[BUF_SIZE];
	client_buf *cbuf;
	cbuf = pool.clients[connfd].readbuf;
	while ((ret = SSL_read(pool.clients[connfd].ssl_info, buf, BUF_SIZE)) != 0) 
	{
		if (ret < 0)
		{
			if (errno == EINTR)
				continue;
			else if (errno == EAGAIN)
				return R_SUCCESS;
			else 
			{
				log_write("Failed receiving data from client %d. errno %d\n", connfd, errno);
				return R_FAIL;
			}
		}
		else 
		{
			if (cbuf->buf_length + ret > MAX_REQUEST)
			{
				log_write("Request is too long\n");
				return R_FAIL;
			}
			else 
			{
				if ((cbuf->buf_length + ret) > cbuf->buf_size)
				{
					if (resize_buf(cbuf))
					{
						log_write("Out of memory.\n");
						return R_FAIL;
					}
				}
				memcpy(cbuf->buf + cbuf->buf_length, buf, ret);
				cbuf->buf_length += ret;
			}
		}
	}
	return R_EOF;
}
	
int ssl_write_helper(int connfd)
{
	int len;
	client_buf *buf;
	buf = pool.clients[connfd].writebuf;
	if ((len = SSL_write(pool.clients[connfd].ssl_info, buf->buf,buf->buf_length)) != buf->buf_length)
	{
		log_write("send() failed\n");
		return S_FAIL;
	}
	return S_SUCCESS;
}



int pipe_read(int connfd) // Similar to read_helper
{
    int ret;
    int sfd = p_s[connfd];
    char buf[BUF_SIZE];
    client_buf *cbuf; 
	if (pool.clients[sfd].writebuf == NULL) 
	{
        if ((pool.clients[sfd].writebuf = init_buf()) == NULL)
            return R_FAIL;
    }
    cbuf = pool.clients[sfd].writebuf;

    while ((ret = read(connfd, buf, BUF_SIZE)) != 0) 
	{
        if (ret < 0)
		{
            if (errno == EINTR)
                continue;
            else if (errno == EAGAIN)
                return R_EOF;
            else 
			{
                log_write("Fail receiving data from pipe\n", connfd, strerror(errno));
                return R_FAIL;
            }
        }
        else 
		{
            if ((cbuf->buf_length + ret) > cbuf->buf_size) 
			{
                if (resize_buf(cbuf)) 
                    return R_FAIL;
            }
            memcpy(cbuf->buf + cbuf->buf_length, buf, ret);
            cbuf->buf_length += ret;
        }
    }   
    return R_EOF;
}
