#include "daemon.h"


/// Obtained from CMU course website !!! 
void signal_handler(int sig)
{
        switch (sig)
        {
                case SIGHUP:
                        break;
                case SIGTERM:
                        close_server(EXIT_SUCCESS);
                        break;
                default:
                        break;
        }
}

int daemonize(char* lock_file)
{
        /* drop to having init() as parent */
        int i, lfp, pid = fork();
        char str[256] = {0};
        if (pid < 0) exit(EXIT_FAILURE);
        if (pid > 0) exit(EXIT_SUCCESS);

        setsid();

        for (i = getdtablesize(); i >= 0; i--)
                close(i);

        i = open("/dev/null", O_RDWR);
        dup(i); /* stdout */
        dup(i); /* stderr */
        umask(027);

        lfp = open(lock_file, O_RDWR|O_CREAT|O_EXCL, 0640);

        if (lfp < 0) { 
                exit(EXIT_FAILURE);
        }

        if (lockf(lfp, F_TLOCK, 0) < 0) {
                exit(EXIT_FAILURE);
        }

        /* only first instance continues */
        sprintf(str, "%d\n", getpid());
        write(lfp, str, strlen(str));   /* record pid to lockfile */

        signal(SIGCHLD, SIG_IGN);
        signal(SIGHUP, signal_handler); /* hangup signal */
        signal(SIGTERM, signal_handler);        /* software termination signal from kill */

        return EXIT_SUCCESS;
}
