#ifndef CGI_HANDLER_H_
#define CGI_HANDLER_H_

#include "client_handler.h"
#define C_PATH "/cgi/"

int cgi_helper(int, client_state *, void *);
#endif
